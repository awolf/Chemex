<?php
return [
    'labels' => [
        'ServiceTrack' => '服务追踪',
        'Service' => '服务',
        'Device' => '设备'
    ],
    'fields' => [
        'service' => [
            'name' => '服务'
        ],
        'device' => [
            'name' => '设备'
        ],
    ],
    'options' => [
    ],
];
