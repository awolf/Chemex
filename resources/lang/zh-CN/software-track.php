<?php
return [
    'labels' => [
        'SoftwareTrack' => '软件追踪',
    ],
    'fields' => [
        'software' => [
            'name' => '软件'
        ],
        'device' => [
            'name' => '设备'
        ],
    ],
    'options' => [
    ],
];
